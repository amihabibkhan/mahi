<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


// Submit Application

Route::get('/v2/submit/application','ApplicationController@index')->name('submitApplication');
Route::post('application/submit','ApplicationController@postSubmitApplication')->name('application.submit');
Route::get('application/delete/{id}','ApplicationController@deleteApplication')->name('deleteApplication');
Route::get('application/approve/{id}/{status}','ApplicationController@approveApplication')->name('approveApplication');

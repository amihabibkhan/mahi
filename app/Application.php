<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    protected $fillable = ['to','through','subject','description', 'user_id', 'status'];

    /*
     * status = 1 = Pending at CSE Office
     * status = 2 = Pending at CSE Head
     * status = 3 = Pending at Dean Office
     * status = 4 = Approved
     * status = 5 = Rejected By CSE Office
     * status = 6 = Rejected By CSE Head
     * status = 7 = Rejected By Dean Office
     *
     * */
}

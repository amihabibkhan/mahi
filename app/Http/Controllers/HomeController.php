<?php

namespace App\Http\Controllers;

use App\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (Auth::user()->type == 4){
            $complains = Application::where('user_id', Auth::id())->get();
        }elseif(Auth::user()->type == 3){
            $complains = Application::where('user_id', Auth::id())->get();
        }elseif (Auth::user()->type == 2){
            $complains = Application::all();
        }else{
            $complains = Application::where('user_id', Auth::id())->get();
        }
        return view('home', compact('complains'));
    }
}

<?php

namespace App\Http\Controllers;

use App\Application;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;

class ApplicationController extends Controller
{
    public function index()
    {
        return view('v2.application.application');
    }

    // post submit
    public function postSubmitApplication(Request $request)
    {
        $request->validate([
            'to' => 'required',
            'through' => 'required',
            'subject' => 'required',
            'description' => 'required',
        ]);
        Application::create([
            'to' => $request->to,
            'through' => $request->through,
            'subject' => $request->subject,
            'description' => $request->description,
            'status' => 1,
            'user_id' => Auth::id(),
        ]);
        return back()->with('success', "Application Submitted Successfully");
    }

    // delete application
    public function deleteApplication($id)
    {
        if (Application::findOrFail($id)->status == 1 && Auth::user()->type == 1){
            Application::findOrFail($id)->delete();
            return back()->with('success', "Application Deleted Successfully");
        }
        return back()->with('error', "Application already submitted. You are unable to delete this application");
    }

    // approve application
    public function approveApplication($id, $command)
    {
        if (Auth::user()->type == 2){
            if ($command == 1){
                Application::findOrFail($id)->update(['status' => 2]);
                $msg = "Application Approved Successfully";
            }else{
                Application::findOrFail($id)->update(['status' => 5]);
                $msg = "Application Rejected Successfully";
            }
        }elseif(Auth::user()->type == 3){
            if ($command == 1){
                Application::findOrFail($id)->update(['status' => 3]);
                $msg = "Application Approved Successfully";
            }else{
                Application::findOrFail($id)->update(['status' => 6]);
                $msg = "Application Rejected Successfully";
            }
        }else{
            if ($command == 1){
                Application::findOrFail($id)->update(['status' => 4]);
                $msg = "Application Approved Successfully";
            }else{
                Application::findOrFail($id)->update(['status' => 7]);
                $msg = "Application Rejected Successfully";
            }
        }
        return back()->with('success', $msg);
    }
}

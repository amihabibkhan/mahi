@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h2>Dashboard</h2>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{-- student dasboard start --}}
                    @if(Auth::user()->type == 1)
                    <table class="table text-center table-bordered">
                        <tr>
                            <th>Application Id</th>
                            <th>Subject</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        @forelse($complains as $complain)
                            <tr>
                                <td>#DIUCSE19-{{ $complain->id }}02</td>
                                <td>{{ $complain->subject }}</td>
                                <td>
                                    @if($complain->status == 1)
                                        Pending at CSE Office
                                    @elseif($complain->status == 2)
                                        Pending at CSE Head
                                    @elseif($complain->status == 3)
                                        Pending at Dean Office
                                    @elseif($complain->status == 4)
                                        <p class="btn btn-success mb-0 ">Approved</p>
                                    @elseif($complain->status == 5)
                                        Rejected By CSE Office
                                    @elseif($complain->status == 6)
                                        Rejected By CSE Head
                                    @else
                                        Rejected By Dean Office
                                    @endif

                                </td>
                                <td>
                                    {{-- view --}}
                                    <a href="" class="btn btn-primary mr-2" data-toggle="modal" data-target="#view{{ $complain->id }}">View</a>
                                    <!-- Button trigger modal -->

                                    <!-- Modal -->
                                    <div class="modal text-left fade" id="view{{ $complain->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLongTitle">{{ $complain->subject }}</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <p><b>To: </b>{{ $complain->to }}</p>
                                                    <hr>
                                                    <p><b>Through: </b>{{ $complain->through }}</p>
                                                    <hr>
                                                    <p>{{ $complain->description }}</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    {{-- delete --}}
                                    @if($complain->status == 1)
                                        <a href="{{ route('deleteApplication', $complain->id) }}" class="btn btn-danger" onclick="return confirm('Are you sure to delete?')">Delete</a>
                                    @endif
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="4"> No Complain Found</td>
                            </tr>
                        @endforelse
                    </table>
                    @endif
                    {{-- student dasboard end --}}

                    {{-- cse office dashboard start --}}
                    @if(Auth::user()->type == 2)
                            <table class="table text-center table-bordered">
                                <tr>
                                    <th>Application Id</th>
                                    <th>Subject</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                @forelse($complains as $complain)
                                    <tr>
                                        <td>#DIUCSE19-{{ $complain->id }}02</td>
                                        <td>{{ $complain->subject }}</td>
                                        <td>
                                            @if($complain->status == 1)
                                                Pending at CSE Office
                                            @elseif($complain->status == 2)
                                                Pending at CSE Head
                                            @elseif($complain->status == 3)
                                                Pending at Dean Office
                                            @elseif($complain->status == 4)
                                                <p class="btn btn-success mb-0 ">Approved</p>
                                            @elseif($complain->status == 5)
                                                Rejected By CSE Office
                                            @elseif($complain->status == 6)
                                                Rejected By CSE Head
                                            @else
                                                Rejected By Dean Office
                                            @endif

                                        </td>
                                        <td>
                                            {{-- view --}}
                                            <a href="" class="btn btn-primary mr-2" data-toggle="modal" data-target="#view{{ $complain->id }}">View</a>
                                            <!-- Button trigger modal -->

                                            <!-- Modal -->
                                            <div class="modal text-left fade" id="view{{ $complain->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLongTitle">{{ $complain->subject }}</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p><b>To: </b>{{ $complain->to }}</p>
                                                            <hr>
                                                            <p><b>Through: </b>{{ $complain->through }}</p>
                                                            <hr>
                                                            <p>{{ $complain->description }}</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            {{-- Approve --}}
                                            <a href="{{ route('approveApplication', [$complain->id, 1]) }}" class="btn btn-dark mr-2" onclick="return confirm('Are you sure to Approve?')">Approve</a>
                                            <a href="{{ route('approveApplication', [$complain->id, 2]) }}" class="btn btn-danger" onclick="return confirm('Are you sure to Reject?')">Reject</a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="4"> No Complain Found</td>
                                    </tr>
                                @endforelse
                            </table>
                    @endif
                    {{-- cse office dashboard end --}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

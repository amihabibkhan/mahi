@extends('layouts.app')

@section('content')

    @if(count($errors) > 0)
        <script>
            alert("You have to fill all fields")
        </script>
    @endif

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h2 class="text-center">Submit Application</h2>
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                            <form action="{{ route('application.submit') }}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label class="font-weight-bold" for="">To</label>
                                    <select name="to" id="" class="form-control">
                                        <option value="CSE Office">CSE Office</option>
                                        <option value="CSE Head">CSE Head</option>
                                        <option value="Dean Office">Dean Office </option>
                                    </select>
                                </div> <div class="form-group">
                                    <label class="font-weight-bold" for="">Through</label>
                                    <select name="through" id="" class="form-control">
                                        <option value="CSE Head">CSE Head</option>
                                        <option value="Dean Office">Dean Office </option>
                                    </select>
                                </div>
                                    <div class="form-group">
                                    <label class="font-weight-bold" for="">Subject</label>
                                    <input type="text" class="form-control" name="subject" placeholder="Write your subject here">
                                </div>

                                <div class="form-group">
                                    <label class="font-weight-bold" for="">Application</label>
                                    <textarea name="description" id="" class="form-control"></textarea>
                                </div>
                                <div class="form-group text-center">
                                    <button class="btn btn-success" type="submit">Submit Application</button>
                                </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
